import React = require('react');

import { Blank } from 'Blank';
import { HashRouter, Route, Switch } from 'react-router-dom';

class Main extends React.Component<{}, {}>
{
	private get router(): JSX.Element
	{
		return (
			<HashRouter hashType="hashbang">
				<Switch>
					<Route exact path="/" component={ Blank } />
				</Switch>
			</HashRouter>
		);
	}

	render(): React.ReactNode
	{
		return (
			<div className="page">
				{ this.router }
			</div>
		);
	}
}

export = <Main/>;