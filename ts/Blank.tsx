import classes = require('classnames');
import Dom = require('react-dom');
import React = require('react');

import { RouteComponentProps } from 'react-router';

const styles = require('./example.module.sass');

interface BlankProps extends RouteComponentProps<{}> {}

interface BlankState
{
}

export class Blank extends React.Component<BlankProps, BlankState>
{
	constructor(props: BlankProps)
	{
		super(props);

		this.state = {
		};
	}

	render(): React.ReactNode
	{
		return (
			<div className="my-component">
				<div className="my-component__title">
					Hello React
				</div>
				<div className={ styles['css-modules-style'] }>
					Block with css-modules
				</div>
			</div>
		);
	}
}