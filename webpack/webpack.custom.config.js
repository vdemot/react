let webpack = require('webpack');
let ExtractTextPlugin = require('extract-text-webpack-plugin');
let autoprefixer = require('autoprefixer');
let UglifyJSPlugin = require('uglifyjs-webpack-plugin');
let Path = require('path');

module.exports = function(options) {
	let fileLoader = {
		loader: 'file-loader',
		options: {
			name: '[path][name].[ext]'
		}
	};

	let plugins = [
		new ExtractTextPlugin({
			filename: './static/css/style.css'
		})
	];

	if ( options.showProgress ) {
		plugins.push(new webpack.ProgressPlugin());
	}

	if ( options.production ) {
		plugins.push(
			new webpack.DefinePlugin({
				'process.env': {
					NODE_ENV: '"production"'
				}
			}),

			new webpack.optimize.UglifyJsPlugin({
				sourceMap: options.sourceMap,
				comments: !options.cutComments,
				compress: {
					warnings: false,
					drop_console: options.dropConsole
				}
			})
		);
	}

	return {
		entry: ['babel-polyfill', __dirname + '/' + options.siteName + '.js'],
		output: {
			path: Path.resolve(__dirname, '../dist', options.distFolder),
			filename: './static/js/main.js'
		},
		devtool: !options.production ? 'source-map' : false,
		mode: options.production ? 'production' : 'development',
		optimization: {
			minimize: options.production,
			minimizer: [
				new UglifyJSPlugin({
					sourceMap: !options.production,
					uglifyOptions: {
						warnings: false,
						output: {
							beautify: options.production,
							comments: !options.production
						}
					}
				})
			]
		},
		performance: {
			hints: false
		},
		devServer: {
			host: '0.0.0.0',
			port: options.devServerPort,
			inline: false,
			stats: {
				hash: true,
				version: true,
				timings: true,
				assets: false,
				chunks: false,
				modules: false,
				reasons: false,
				children: false,
				source: false,
				errors: true,
				errorDetails: true,
				warnings: true
			}
		},
		resolve: {
			modules: ['node_modules', 'ts'],
			extensions: ['.ts', '.tsx', '.js']
		},
		module: {
			rules: [
				{
					test: /\.tsx?$/,
					use: [
						{
							loader: 'babel-loader',
							options: {
								cacheDirectory: true,
								presets: [
									["env", {
										targets: {
											browsers: ["last 2 versions", "safari >= 8"]
										},
										debug: options.production,
										useBuiltIns: false
									}]
								]
							}
						},
						'ts-loader'
					],
					exclude: /node_modules/
				},
				{
					test: /\.sass$/,
					exclude: Path.resolve(__dirname, '../ts'),
					use: ExtractTextPlugin.extract({
						fallback: 'style-loader',
						use: [
							{
								loader: 'css-loader',
								options: {
									sourceMap: options.sourceMap
								}
							},
							{
								loader: 'postcss-loader',
								options: {
									plugins: [autoprefixer({ browsers: ['last 2 versions', '> 5%'], grid: true })],
									sourceMap: options.sourceMap
								}
							},
							{
								loader: 'sass-loader',
								options: {
									sourceMap: options.sourceMap
								}
							}
						]
					})
				},
				{
					test:/\.module\.sass$/,
					use: ExtractTextPlugin.extract({
						fallback: 'style-loader',
						use: [
							{
								loader: 'css-loader',
								options: {
									sourceMap: options.sourceMap,
									localIdentName: '[local]--[hash:base64:5]',
									modules: true,
									importLoaders: true
								}
							},
							{
								loader: 'postcss-loader',
								options: {
									plugins: [autoprefixer({ browsers: ['last 2 versions', '> 5%'], grid: true })],
									sourceMap: options.sourceMap
								}
							},
							{
								loader: 'sass-loader',
								options: {
									sourceMap: options.sourceMap
								}
							}
						]
					})
				},
				{
					test: /\.(svg|png|jpg|gif|ico)$/,
					use: fileLoader
				}
			]
		},
		plugins: plugins
	}
};